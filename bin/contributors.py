#!/usr/bin/env python3

import subprocess
import yaml

# eg. https://salsa.debian.org/reproducible-builds/reproducible-presentations/-/blob/master/2021-08-24-where-we-come-from-and-where-we-are-going/index.html
EXTRA = {
    "akira",
    "Alexander Couzens (lynxis)",
    "Alexis Bienvenüe",
    "Allan Gunn (gunner)",
    "Andrew Ayer",
    "Asheesh Laroia",
    "Ben Hutchings",
    "Boyuan Yang",
    "Ceridwen",
    "Christoph Berg",
    "Chris West",
    "Clint Adams",
    "Dafydd Harries",
    "Daniel Stender",
    "David Suarez",
    "Drew Fisher",
    "Ed Maste",
    "Elio Qoshi",
    "Emmanuel Bourg",
    "Esa Peuha",
    "Fabian Keil",
    "Fabian Wolff",
    "Guillem Jover",
    "Hannes Mehnert",
    "Harlan Lieberman-Berg",
    "Helmut Grohne",
    "HW42",
    "Jan Nieuwenhuizen",
    "Jelmer Vernooij",
    "Juan Picca",
    "Justin Cappos",
    "Levente Polyak",
    "Ludovic Courtès",
    "Marcus Hoffmann (bubu)",
    "Maria Glukhova",
    "Mathieu Bridon",
    "Morten Linderud",
    "Nicolas Boulenguez",
    "Niels Thykier",
    "Omar Navarro Leija",
    "Paul Wise",
    "Peter De Wachter",
    "Philip Rinn",
    "Robbie Harwood",
    "Santiago Vila",
    "Sascha Steinbiss",
    "Satyam Zode",
    "Scarlett Clark",
    "Stefano Rivera",
    "Stéphane Glondu",
    "Steven Chamberlain",
    "Tom Fitzhenry",
    "Valentin Lorentz",
    "Wookey",
}

NICKNAMES = {
    ("Holger Levsen", "h01ger"),
    ("Jeremy Bobbio", "lunar"),
}


def get_git_authors():
    output = subprocess.check_output(("git", "log", "--format=%aN|%aE"))
    authors = set()
    emails = set()

    for line in output.decode("utf-8").split("\n"):
        if line:
            name, email = line.rsplit("|", 1)
            if email not in emails:
                emails.add(email)
                authors.add(name)

    return authors


def main():
    data = get_git_authors()
    data.update(EXTRA)

    for x, y in NICKNAMES:
        try:
            data.remove(x)
            data.add(f"{x} ({y})")
        except KeyError:
            pass

    with open("_data/contributors.yml", "w") as f:
        yaml.dump(sorted(list(data)), f)


if __name__ == "__main__":
    main()
