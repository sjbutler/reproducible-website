---
title: Academic publications
layout: docs
permalink: /docs/publications/
---

* *Trusting Trust - Reflections on Trusting Trust* (1984) — Ken Thompson. ([PDF](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf))

* *Fully Countering Trusting Trust through Diverse Double-Compiling* (2005/2009) — David A. Wheeler ([PDF](https://dwheeler.com/trusting-trust/dissertation/wheeler-trusting-trust-ddc.pdf), [...](https://dwheeler.com/trusting-trust/))

* *Functional Package Management with Guix* (2013) — Ludovic Courtès. [[...](https://hal.inria.fr/hal-00824004/en)]

* *Reproducible and User-Controlled Software Environments in HPC with Guix* (2015) — Ludovic Courtès, Ricardo Wurmus [[...](https://hal.inria.fr/hal-01161771/en)]

* *in-toto: Providing farm-to-table guarantees for bits and bytes* (2019) — Santiago Torres-Arias, New York University; Hammad Afzali, New Jersey Institute of Technology; Trishank Karthik Kuppusamy, Datadog; Reza Curtmola, New Jersey Institute of Technology; Justin Cappos, New York University. ([PDF](https://www.usenix.org/system/files/sec19-torres-arias.pdf))

* *Backstabber's Knife Collection: A Review of Open Source Software Supply Chain Attacks* (2020) — Marc Ohm, Henrik Plate, Arnold Sykosch, Michael Meier. ([PDF](https://arxiv.org/pdf/2005.09535.pdf))

* *Automated Localization for Unreproducible Builds* (2018) — Zhilei Ren, He Jiang, Jifeng Xuan, Zijiang Yang. ([PDF](https://arxiv.org/pdf/1803.06766.pdf))

* *Reproducible Containers* (2020) — Navarro Leija, Omar S. and Shiptoski, Kelly and Scott, Ryan G. and Wang, Baojun and Renner, Nicholas and Newton, Ryan R. and Devietti, Joseph. ([...](https://dl.acm.org/doi/10.1145/3373376.3378519))

* *Towards detection of software supply chain attacks by forensic artifacts* (2020) — Marc Ohm, Arnold Sykosch, Michael Meier. ([Link](https://dl.acm.org/doi/10.1145/3407023.3409183))

* *Reproducible builds: Increasing the integrity of software supply chains.* (2021) — Chris Lamb & Stefano Zacchiroli. ([Link](https://ieeexplore.ieee.org/document/9403390/))

* *On business adoption and use of reproducible builds for open and closed source software* (2022) — Simon Butler, Jonas Gamalielsson, Björn Lundell, Christoffer Brax, Anders Mattsson, Tomas Gustavsson, Jonas Feist, Bengt Kvarnström & Erik Lönroth. ([Link](https://link.springer.com/article/10.1007/s11219-022-09607-z)

